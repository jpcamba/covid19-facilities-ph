## Map of Available COVID-19 Facilities in the Philippines

View map [here](http://jpcamba.gitlab.io/covid19-facilities-ph). Map will be updated every Friday.

*Map is created using [HERE Studio](https://xyz.here.com/studio/) for [#HackForBetterDays hackathon](https://developer.here.com/hackforbetterdays). Data is courtesy of DOH.*